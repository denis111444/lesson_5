﻿using System;

namespace Lesson_5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Расчет суммы двух чисел");
            Console.WriteLine("Введите первое число :");
            int a1 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Введите второе число :");
            int b1 = Convert.ToInt32(Console.ReadLine());
            int result1 = a1 + b1;
            Console.WriteLine($"Сумма двух чисел = {result1}");

            Console.WriteLine("Расчет площади прямоугольника");
            Console.WriteLine("Введите длину прямоугольника :");
            int a2 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Введите ширину прямоугольника :");
            int b2 = Convert.ToInt32(Console.ReadLine());
            int result2 = a2 * b2;
            Console.WriteLine($"Площадь прямоугольника = {result2}");

            Console.WriteLine("Расчет площади круга");
            Console.WriteLine("Введите радиус круга :");
            int a3 = Convert.ToInt32(Console.ReadLine());
            float b3 = 3.14f;
            float result3 =(a3 *a3)* b3;
            Console.WriteLine($"Площадь прямоугольника = {result3}");

            Console.WriteLine("Определние четности/нечетности числа");
            Console.WriteLine("Введите число : ");
            int a4 = Convert.ToInt32(Console.Read());
            if (a4 % 2 == 0) Console.WriteLine("Четное");
            else Console.WriteLine("Нечетное");

            float[] numbers = { 5.5f, 8, 2.3f, 4.6f, 6 };
            float summ = numbers[1] + numbers[2] + numbers[3] + numbers[4] + numbers[0];
            Console.WriteLine("Сумма элементов массива равна = " + summ);

        }
    }
}
